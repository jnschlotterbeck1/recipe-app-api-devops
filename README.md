# Recipe App API DevOps Starting Point

Source code for my Udemy course Build a [Backend REST API with Python & Django - Advanced](http://udemy.com/django-python-advanced/).

The course teaches how to build a fully functioning REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development

## Getting started

To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000

## Aws-vault

```
aws-vault list
aws-vault exec <user> --duration=12h
```

## Connect to Bastion

```
ssh ec2-user@< Public IPv4 DNS >
```

## Bastion Commands

### Authenticate with ECR

In order to pull the application image, authentication with ECR is required.

To authenticate with ECR:

```sh
$(aws ecr get-login --no-include-email --region us-east-1)
```

### Create a superuser 

Replace the following variables:
 * `<DB_HOST>`: The hostname for the database (retrieve from Terraform apply output)
 * `<DB_PASS>`: The password for the database instance (retrieve from GitLab CI/CD variables)
 * Also, you can see all the DB values in ECS
 * `<ECR_REPO>`: ECR recipe-app-api-devops URI

```bash
docker run -it \
    -e DB_HOST=<DB_HOST> \
    -e DB_NAME=recipe \
    -e DB_USER=recipeapp \
    -e DB_PASS=<DB_PASS> \
    <ECR_REPO>:latest \
    sh -c "python manage.py wait_for_db && python manage.py createsuperuser"
```
